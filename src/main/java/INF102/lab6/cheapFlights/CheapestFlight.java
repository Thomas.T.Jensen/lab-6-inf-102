package INF102.lab6.cheapFlights;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {

    // TODO: Implement the methods in the interface ICheapestFlight
    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) { // O(n)
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>(); // O(1)

        for (Flight flight : flights) { // O(n)
            // Since the DirectedGraph class does not support checking for the existence of
            // a vertex directly,
            // we try to add the vertex, and the addVertex method will only add it if it's
            // not already present.
            graph.addVertex(flight.start); // O(1)
            graph.addVertex(flight.destination); // O(1)

            // Add the edge with the associated cost.
            // The addEdge method in WeightedDirectedGraph will add the edge if it's not
            // already present.
            graph.addEdge(flight.start, flight.destination, flight.cost); // O(1)
        }

        return graph; // O(1)
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) { // O(n)

        // Construct the graph
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights); // O(n)

        // Map to keep track of the minimum cost to reach each city with a given number
        // of stops
        Map<City, Map<Integer, Integer>> costAtStops = new HashMap<>(); // O(1)

        // Priority queue to explore the nodes with the lowest cost first
        PriorityQueue<Node> queue = new PriorityQueue<>(Comparator.comparingInt(node -> node.cost)); // O(1)

        // Start by adding the start city to the queue with a cost of 0 and -1 stops
        // (since the starting city isn't considered a stop)
        queue.offer(new Node(start, 0, -1)); // O(1)

        while (!queue.isEmpty()) { // O(n)
            Node current = queue.poll(); // O(1)

            // If we've reached the destination with nMaxStops or fewer, return the cost
            if (current.city.equals(destination) && current.stops <= nMaxStops) { // O(1)
                return current.cost; // O(1)
            }

            // Skip if we've exceeded the maximum number of stops
            if (current.stops >= nMaxStops) { // O(1)
                continue; // O(1)
            }

            // Explore the neighbors
            for (DirectedEdge<City> edge : graph.outArcs(current.city)) { // O(n)
                int newCost = current.cost + graph.getWeight(edge.from, edge.to); // O(1)
                int newStops = current.stops + 1; // O(1)

                // Initialize the inner map if necessary
                costAtStops.putIfAbsent(edge.to, new HashMap<>()); // O(1)

                // If this path is cheaper or we haven't visited this city with this number of
                // stops, update the cost and continue searching
                if (!costAtStops.get(edge.to).containsKey(newStops) // O(1)
                        || newCost < costAtStops.get(edge.to).get(newStops)) { // O(1)
                    costAtStops.get(edge.to).put(newStops, newCost); // O(1)
                    queue.offer(new Node(edge.to, newCost, newStops)); // O(1)
                }
            }
        }

        // If the destination is not reachable within the maximum number of stops,
        // return -1 or any other indicator of failure
        return -1; // O(1)
    }

    private static class Node { // O(1)
        City city; // O(1)
        int cost; // O(1)
        int stops; // O(1)

        Node(City city, int cost, int stops) { // O(1)
            this.city = city; // O(1)
            this.cost = cost; // O(1)
            this.stops = stops; // O(1)
        }
    }

}
